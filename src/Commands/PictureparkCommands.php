<?php

namespace Drupal\picturepark\Commands;

use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drush\Commands\DrushCommands;

/**
 * Picturepark Command.
 */
class PictureparkCommands extends DrushCommands {

  /**
   * Command description here.
   *
   * @usage picturepark:sync-assets
   *   Sync assets from picturepark into Drupal.
   *
   * @command picturepark:sync-assets
   * @aliases ppsa
   */
  public function pictureparkSyncAssets() {
    $this->logger()->success(dt('Syncing Picturepark assets.'));
  }

}
