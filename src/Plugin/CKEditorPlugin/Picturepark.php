<?php

namespace Drupal\picturepark\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "Picturepark" plugin.
 *
 * @CKEditorPlugin(
 *   id = "picturepark",
 *   label = @Translation("Picturepark"),
 *   module = "picturepark"
 * )
 */
class Picturepark extends CKEditorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return drupal_get_path('module', 'picturepark') . '/js/plugins/picturepark/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    $module_path = drupal_get_path('module', 'picturepark');
    return [
      'picturepark' => [
        'label' => $this->t('Picturepark'),
        'image' => $module_path . '/js/plugins/picturepark/icons/picturepark.png',
      ],
    ];
  }

}
